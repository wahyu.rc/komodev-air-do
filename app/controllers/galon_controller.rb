class GalonController < ApplicationController
	def index
		@galons = Galon.all
	end

	def show
		@galond = Galon.find(params[:id])
	end

	def new
		@galon = Galon.new
	end

	def create
		@galon = Galon.new(galon_params)
		if @galon.save
			redirect_to @galon
		else
			redirect_to '/gform'
		end
	end

	private
	def galon_params
		params.require(:galon).permit(:jgalon, :telp, :alamat)
	end
end
