class TangkiController < ApplicationController
	def index
		@tangkis = Tangki.all
	end

	def show
		@tangkid = Tangki.find(params[:id])
	end
	
	def new
		@tangki = Tangki.new
	end

	def create
		@tangki = Tangki.new(tangki_params)
		if @tangki.save
			redirect_to @tangki
		else
			redirect_to '/tform'
		end
	end

	private
	def tangki_params
		params.require(:tangki).permit(:liter, :telp, :alamat)
	end
end