class User < ApplicationRecord
	has_secure_password
	validates :fnama, presence: true
    validates :nama, presence: true
    validates :telp, presence: true, :numericality => {:only_integer => true}
    validates :alamat, presence: true
    validates :password, confirmation: true, length: { minimum: 8 }
end
