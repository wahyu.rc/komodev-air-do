class CreateUsers < ActiveRecord::Migration[5.2]
  def change
    create_table :users do |t|
      t.string :fnama
      t.string :nama
      t.string :telp
      t.text :alamat
      t.string :password_digest

      t.timestamps
    end
  end
end
