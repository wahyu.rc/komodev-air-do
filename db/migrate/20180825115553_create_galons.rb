class CreateGalons < ActiveRecord::Migration[5.2]
  def change
    create_table :galons do |t|
      t.integer :jgalon
      t.string :telp
      t.text :alamat

      t.timestamps
    end
  end
end
